package datastructure;

import cellular.CellState;

import java.util.Arrays;
import java.util.Optional;
import java.util.Vector;

public class CellGrid implements IGrid {

    int cols;
    int rows;
    CellState[][] grid;

    public CellGrid(int rows, int columns, CellState initialState) {
        this.rows = rows;
        this.cols = columns;

        CellState[][] cellgrid = new CellState[rows][columns];

        for (int row = 0; row < rows; row++) {
            for (int col = 0; col < cols; col++) {
                cellgrid[row][col] = initialState;
                this.grid = cellgrid;
            }
        }

    }

    @Override
    public int numRows() {
        return this.rows;
    }

    @Override
    public int numColumns() {
        return this.cols;
    }

    @Override
    public void set(int row, int column, CellState element) {

        if (row > 0 || row <= this.rows || column > 0 || column <= this.cols) {
            this.grid[row][column] = element;

        } else {
            throw new IndexOutOfBoundsException();
        }
    }

    @Override
    public CellState get(int row, int column) {

        if (row > 0 || row <= this.rows || column > 0 || column <= this.cols) {
            return grid[row][column];

        } else {
            throw new IndexOutOfBoundsException();
        }

    }

    @Override
    public IGrid copy() {
        IGrid copyGrid = new CellGrid(this.rows, this.cols, CellState.DEAD);
        for (int row = 0; row < rows; row++) {
            for (int col = 0; col < cols; col++) {
                copyGrid.set(row, col, get(row, col));
            }

        }
        return copyGrid;
    }

}
