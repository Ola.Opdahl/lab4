package cellular;

public class BriansBrain extends GameOfLife {
    public BriansBrain(int rows, int columns) {
        super(rows, columns);

    }

    @Override
    public CellState getNextCell(int row, int column) {
        CellState nextgen = CellState.DEAD;
        int numAlive = countNeighbors(row, column, CellState.ALIVE);
        if (currentGeneration.get(row, column) == CellState.ALIVE) {
            nextgen = CellState.DYING;

        }
        if (currentGeneration.get(row, column) == CellState.DYING) {
            nextgen = CellState.DEAD;
        }
        if (currentGeneration.get(row, column) == CellState.DEAD) {
            if (numAlive == 2) {
                nextgen = CellState.ALIVE;
            } else {
                nextgen = CellState.DEAD;
            }
        }
        return nextgen;
    }
}
